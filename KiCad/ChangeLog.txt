Schematic changelog revision 1.1:
Change D204,D205,D206,D207 to SS14 (SOD123)
Change D201,D202,D203 to SS14 (SOD123)
Change U401,U402 to TLE2702
Change U403,U404 to TLE2701
Change mounting holes and added fiducials
Fixed swapped VDD/DGND on ADC
Change R404 and R406 to 1k5
Change U201 to TLV1117LV33DCY
Correct C403, C407 polarity
Add R401, R402
Removed R431, R433
Change C413 and C415 to 330p
Change C447 and C448 to 1n
Change R407-R410 to 5k6
Added note and updated R321-R324 values
Change R411,R413 to 2k2
Change R407-R410 to 2k2
Add R401,R402
Change D208 to 1206
Change C321 to 10uF
Updated all parts with extra details/fields

PCB changelog revision 1.1:
Update connectors with 1mm holes
Change mounting holes
Fix VDD/DGND on ADC
Layout changes to increase part spacing
Added fiducials
Added silkscreen labels on jumpers
Change D201, D202, D203 to SOD123
Moved C319 and C326 to front layer
Correct C403, C407 polarity
Add R401, R402 and change audio input layout
Removed R431, R433
Fix copper pour keep outs around U401-U404
Change D208 to 1206